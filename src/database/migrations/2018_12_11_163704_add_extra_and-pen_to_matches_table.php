<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraAndPenToMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->integer('home_team_extra_goals')->nullable()->after('away_team_goals');
            $table->integer('away_team_extra_goals')->nullable()->after('home_team_extra_goals');
            $table->integer('home_team_pen_goals')->nullable()->after('away_team_extra_goals');
            $table->integer('away_team_pen_goals')->nullable()->after('home_team_pen_goals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matches', function (Blueprint $table) {
            //
        });
    }
}
