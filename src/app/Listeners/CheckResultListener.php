<?php

namespace App\Listeners;

use App\Events\CheckResult;
use App\Match;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Naymin\APIfootball\Facades\APIFootballFacade as Football;
use GuzzleHttp\Client;

class CheckResultListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  CheckResult  $event
     * @return void
     */
    public function handle(CheckResult $event)
    {
        $api = Football::getMatche($event->match->fixture_id)['fixtures'][0];
        if($api->status == "Match Finished")
        {
            $home_team = $api->homeTeam->team_name;
            $away_team = $api->awayTeam->team_name;
            $home_team_goals = $api->goalsHomeTeam;
            $away_team_goals = $api->goalsAwayTeam;

            $league_name = Football::getLeague($api->league_id)['leagues'][0]->name;

            if($api->score->penalty != null)
            {
                $penalty_result = explode('-',$api->score->penalty);
                $home_team_penalty_goals = $penalty_result[0];
                $away_team_penalty_goals = $penalty_result[1];

                $extratime_result = explode('-',$api->score->extratime);
                $home_team_extra_goals = $extratime_result[0];
                $away_team_extra_goals = $extratime_result[1];

                $sms = $league_name.":".$home_team.' '.'('.$home_team_penalty_goals.')'.$home_team_extra_goals.'- '
                    .$away_team_extra_goals.'('.$away_team_penalty_goals.') '.$away_team.' (P)';


            }
            elseif ($api->score->extratime != null)
            {
                $extratime_result = explode('-',$api->score->extratime);
                $home_team_extra_goals = $extratime_result[0];
                $away_team_extra_goals = $extratime_result[1];

                $sms = $league_name.":".$home_team.' '.$home_team_goals.'('.$home_team_extra_goals.') - '
                    .$away_team_goals.'('.$away_team_extra_goals.') '.$away_team. ' (AET)';


            } else {
                $sms = $league_name.': '.$home_team.' '.$home_team_goals.'-'.$away_team_goals.' '.$away_team;
            }
            $client = new Client();
            $response = $client->request('GET', 'http://cms2.blueplanet.com.mm/api/v1/push', [
                'query' => [
                    'serviceid'                 => 5210,
                    'body'                      => $sms,
                    'date'                      => Carbon::now()->addMinute(6)->toDateTimeString(),
                    'email'                     => 'tech.vas@blueplanet.com.mm',
                    'password'                  => 'T_BP_FA$BpPb@01250225fa'
                ]
            ]);

            $res = $response->getBody();
            $body = json_decode($res);
            if($body->status == "success")
            {
                $match = Match::find($event->match->id);
                $match->home_team = $home_team;
                $match->away_team = $away_team;
                $match->home_team_goals = $home_team_goals;
                $match->away_team_goals = $away_team_goals;
                if(isset($home_team_extra_goals) && isset($away_team_extra_goals))
                {
                    $match->home_team_extra_goals = $home_team_extra_goals;
                    $match->away_team_extra_goals = $away_team_extra_goals;
                }
                if(isset($home_team_penalty_goals) && isset($away_team_penalty_goals))
                {
                    $match->home_team_pen_goals = $home_team_penalty_goals;
                    $match->away_team_pen_goals = $away_team_penalty_goals;
                }
                $match->status = "FINISHED";
                $match->save();
            }



        }

    }
}
