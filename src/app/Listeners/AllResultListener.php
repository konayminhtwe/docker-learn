<?php

namespace App\Listeners;

use App\Events\AllResult;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AllResultListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AllResult  $event
     * @return void
     */
    public function handle(AllResult $event)
    {
        $sms = $event->league->name.":";
        foreach ($event->matches as $match)
        {
            if($match->status == "FINISHED")
            {
                if($match->home_team_pen_goals != null && $match->away_team_pen_goals != null)
                {
                    $sms .= $match->home_team.' '.'('.$match->home_team_pen_goals.')'.$match->home_team_extra_goals.'- '
                        .$match->away_team_extra_goals.'('.$match->away_team_pen_goals.") ".$match->away_team." (P);";
                }
                elseif($match->home_team_extra_goals != null && $match->away_team_extra_goals != null)
                {
                    $sms .= $match->home_team." ".$match->home_team_goals.'('.$match->home_team_extra_goals.") -"
                        .$match->away_team_goals.'('.$match->away_team_extra_goals.") ".$match->away_team." (AET);";
                }
                else
                {
                    $sms .= $match->home_team." ".$match->home_team_goals."-".$match->away_team_goals." ".$match->away_team.";";
                }
            }


        }

        do{
            $client = new Client();
            $response = $client->request('GET', 'http://cms2.blueplanet.com.mm/api/v1/push', [
                'query' => [
                    'serviceid'                 => 5220,
                    'body'                      => $sms,
                    'date'                      => Carbon::today()->addHour(6)->toDateTimeString(),
                    'key'                       => 5241,
                    'email'                     => 'tech.vas@blueplanet.com.mm',
                    'password'                  => 'T_BP_FA$BpPb@01250225fa'


                ]
            ]);

            $res = $response->getBody();
            $body = json_decode($res);
        }
        while($body->status != "success");

    }



}
