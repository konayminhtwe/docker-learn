<?php

namespace App\Listeners;

use App\Events\SchduleQuery;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Naymin\APIfootball\Facades\APIFootballFacade as Football;
use App\Match;

class SchduleQueryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AllResult  $event
     * @return void
     */
    public function handle(SchduleQuery $event)
    {
//        $api = Football::getLeagueMatches($event->league->league_id);
        $tomorrow = Carbon::tomorrow()->toDateString();
        $api = Football::getLeagueMatchesWithDate($event->league->league_id,$tomorrow);

        if(!!$api['results'])
        {

            foreach ($api['fixtures'] as $fixture)
            {
                $imported = Match::where('fixture_id', $fixture->fixture_id)
                    ->where('local_time',Carbon::parse($fixture->event_date)->toDateTimeString())
                    ->get()->first();

                if(!$imported)
                {
                    $match = new Match;
                    $match->league_id = $event->league->id;
                    $match->fixture_id = $fixture->fixture_id;
                    $match->local_time = Carbon::parse($fixture->event_date)->toDateTimeString();
                    $match->mm_time = Carbon::parse($fixture->event_date)->setTimezone('Asia/Yangon');
                    $match->home_team = $fixture->homeTeam->team_name;
                    $match->away_team = $fixture->awayTeam->team_name;
                    $match->save();
                }

            }

        }

    }
}
