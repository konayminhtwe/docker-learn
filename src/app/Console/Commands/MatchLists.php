<?php

namespace App\Console\Commands;

use App\League;
use Illuminate\Console\Command;
use Naymin\APIfootball\Facades\APIFootballFacade as Football;
use App\Match;
use Carbon\Carbon;

class MatchLists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:list {league}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrive Match Lists from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $leagueId = $this->argument('league');

        $api = Football::getLeagueMatches($leagueId);
        if(!!$api['results'])
        {
            $league_name = Football::getLeague($leagueId)['leagues'][0]->name;
            $league = new League;
            $league->league_id = $leagueId;
            $league->name = $league_name;
            $league->status =1;
            $league->save();

            foreach ($api['fixtures'] as $fixture)
            {
                $match = new Match;
                $match->league_id = $league->id;
                $match->fixture_id = $fixture->fixture_id;
                $match->local_time = Carbon::parse($fixture->event_date)->toDateTimeString();
                $match->mm_time = Carbon::parse($fixture->event_date)->setTimezone('Asia/Yangon');
                $match->home_team = $fixture->homeTeam->team_name;
                $match->away_team = $fixture->awayTeam->team_name;
                $match->save();


            }
            echo "All Matches have been imported"."\n";

        }
        else{
            echo "No Matches have been imported"."\n";
        }

    }
}
