<?php

namespace App\Console;

use App\Events\AllResult;
use App\Events\CheckResult;
use App\Events\SchduleQuery;
use App\League;
use App\Match;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            $leagues = League::all();
            foreach ($leagues as $league)
            {
                event(new SchduleQuery($league));

            }

        })->dailyAt('17:00');

        $schedule->call(function(){
            $end = Carbon::now();
            $start = Carbon::parse($end)->subHours(4);
            $matches = Match::whereBetween('mm_time',[$start,$end])
                ->get();
            foreach($matches as $match)
            {
                if($match->status != "FINISHED")
                {
                    event(new CheckResult($match));
                }
            }
        })->everyFiveMinutes();

        $schedule->call(function(){

            $today = Carbon::today()->addMinutes(345);
            $yesterday = Carbon::yesterday()->addMinutes(255);

            $leagues = League::all();
            foreach ($leagues as $league) {
                $valid_match = Match::where('league_id',$league->id)
                    ->whereBetween('mm_time',[$yesterday,$today])
                    ->first();
                if($valid_match)
                {
                    $matches = Match::where('league_id',$league->id)
                        ->whereBetween('mm_time', [$yesterday, $today])
                        ->get();
                    event(new AllResult($matches,$league));
                }
            }

        })->dailyAt('05:50');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
