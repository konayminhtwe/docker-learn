<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\League;
use App\Match;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Naymin\APIfootball\Facades\APIFootballFacade as Football;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test',function(){
//    dd(Football::getMatche(117904)['fixtures'][0]);
    dd(explode('-',Football::getMatche(169548)['fixtures'][0]));
    dd(explode('-',Football::getMatche(169548)['fixtures'][0]->score->penalty));
//    dd(Football::getLeagues());
//    dd(Football::getLeague(194)['leagues'][0]);
//    dd(Football::getLeagueStandings(2));
//    dd(Football::getLeagueMatches(194)['fixtures']);
//    dd(Football::getLeagueMatches(194777777));
//    dd(Football::getTeam(42));
//    dd(Football::getMatchesForTeam(42));

});


Route::get('/schedule',function(){

    $api = Football::getLeagueMatches(194);
    if(!!$api['results'])
    {
        foreach ($api['fixtures'] as $fixture)
        {
//            $match = new Match;
            $match = new \stdClass;
            $match->fixture_id = $fixture->fixture_id;
            $match->local_time = Carbon::parse($fixture->event_date)->toDateTimeString();
            $match->mm_time = Carbon::parse($fixture->event_date)->setTimezone('Asia/Yangon');
            $match->home_team = $fixture->homeTeam->team_name;
            $match->away_team = $fixture->awayTeam->team_name;
            var_dump($match);
//            $match->save();

            echo "All Matches have been imported"."\n";
        }

    }
    else{
        echo "No Matches have been imported"."\n";
    }

});

Route::get('/match',function(){

    $api = Football::getMatche(169548)['fixtures'][0];
    if($api->status == "Match Finished")
    {
        $home_team = $api->homeTeam->team_name;
        $away_team = $api->awayTeam->team_name;
        $home_team_goals = $api->goalsHomeTeam;
        $away_team_goals = $api->goalsAwayTeam;

        $league_name = Football::getLeague($api->league_id)['leagues'][0]->name;

        if($api->score->penalty != null)
        {
            $penalty_result = explode('-',$api->score->penalty);
            $home_team_penalty_goals = $penalty_result[0];
            $away_team_penalty_goals = $penalty_result[1];

            $extratime_result = explode('-',$api->score->extratime);
//            $home_team_extra_goals = $extratime_result[0];
//            $away_team_extra_goals = $extratime_result[1];

//            $sms = $league_name.":".$home_team.' '.'('.$home_team_penalty_goals.')'.$home_team_extra_goals.'- '
//                .$away_team_extra_goals.'('.$away_team_penalty_goals.') '.$away_team.' (P)';


        }
        elseif ($api->score->extratime != null)
        {
            $extratime_result = explode('-',$api->score->extratime);
            $home_team_extra_goals = $extratime_result[0];
            $away_team_extra_goals = $extratime_result[1];

            $sms = $league_name.":".$home_team.' '.$home_team_goals.'('.$home_team_extra_goals.') - '
                .$away_team_goals.'('.$away_team_extra_goals.') '.$away_team. ' (AET)';


        } else {
            $sms = $league_name.': '.$home_team.' '.$home_team_goals.'-'.$away_team_goals.' '.$away_team;
        }

        $match = Match::find(393);
        $match->home_team = $home_team;
        $match->away_team = $away_team;
        $match->home_team_goals = $home_team_goals;
        $match->away_team_goals = $away_team_goals;
        if(isset($home_team_extra_goals) && isset($away_team_extra_goals))
        {
            $match->home_team_extra_goals = $home_team_extra_goals;
            $match->away_team_extra_goals = $away_team_extra_goals;
        }
        if(isset($home_team_penalty_goals) && isset($away_team_penalty_goals))
        {
            $match->home_team_pen_goals = $home_team_penalty_goals;
            $match->away_team_pen_goals = $away_team_penalty_goals;
        }
        $match->status = "FINISHED";
        $match->save();




    }



});

Route::get('/list',function(){
    header('Content-Disposition: attachment; filename="5210-5220.csv"');
    $fp = fopen('php://output', 'w');
    $lists = [];
    $matches = Match::all();
    foreach ($matches as $match)
    {
        array_push($lists,[
            $match->home_team,
            $match->away_team,
            $match->mm_time,
            Carbon::parse($match->mm_time)->toDateString(),


        ]);
    }

    foreach($lists as $list)
    {
        fputcsv($fp, $list);
    }
    fclose($fp);


});

Route::get('/eng',function(){
    header('Content-Disposition: attachment; filename="5230.csv"');
    $fp = fopen('php://output', 'w');
    $lists = [];
    $matches = Match::where('league_id',1)->get();
    foreach ($matches as $match)
    {
        array_push($lists,[
            $match->home_team,
            $match->away_team,
            $match->mm_time,
            Carbon::parse($match->mm_time)->toDateString(),


        ]);
    }

    foreach($lists as $list)
    {
        fputcsv($fp, $list);
    }
    fclose($fp);


});

Route::get('tomorrow',function (){
    $tomorrow = Carbon::tomorrow()->addDays(3)->toDateString();
    $api = Football::getLeagueMatchesWithDate(524,$tomorrow);
    dd($api);
});

